<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hard_work');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']nE_kM;mD@p^,!JP?mzCCYgDumD3S^jNT+P58c0o<UUeZ&=Av;/{N0Zj*h<M~lw|');
define('SECURE_AUTH_KEY',  'C_X(s0~W}y28_~[DWqq9o>t?,vZLKn4YHTt!N7L5AxPIcxW}}]O^bli{,$FS91N`');
define('LOGGED_IN_KEY',    'z-s1u~9en@P-}_z^UV1~)3XCjkJBOc)O>V6wYj-[]LvBDe#K6YP4yfO${HbX~{C*');
define('NONCE_KEY',        '%Na<-*sBpg@4x4_EA1RW_kF}ibK HWKKR32<5,p8*`rGIApKOaw`Mk_BU6<[U<k7');
define('AUTH_SALT',        'H7=-oqZWHQMGkPiJ8>(8!ZuHf82a,H>E Z%W &B2T?iNKqYsqk*oO32/)6Y6k*I,');
define('SECURE_AUTH_SALT', '$6@zlL^3j_i|E&BcKa!.wA]4^xT}1Ge[O$V9f.~X^8_+C@*WYvo;r-^WA)XE|*d>');
define('LOGGED_IN_SALT',   ' O?komSu:(ZaU==L6O!qNW1[xU6/m^v8;b/s{a}9,*=Y48cy544D7U~AXsBKB  U');
define('NONCE_SALT',       'r?s{7{Te6V%bMrHeZ4$RQ:S7-{ODPd^tU54YtK#DXW>>.0r1ot:LE~_I3h1O7iCC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pallabwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
